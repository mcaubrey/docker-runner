FROM gitlab/gitlab-runner:latest

# Update repostiry.
RUN apt-get update -q && apt-get install -qqy \
        apt-transport-https \
        curl \
        ca-certificates \
        software-properties-common \
        sudo

# Install Docker Community Edition.
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
RUN apt-get update -q && apt-get install -qqy docker-ce

RUN docker --version

# Install Docker Compose.
RUN curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose

# Check that Docker Compose has been installed correctly.
RUN docker-compose --version

RUN echo "gitlab-runner ALL=(ALL)    NOPASSWD: ALL" >> /etc/sudoers
RUN echo "sudo chown gitlab-runner /var/run/docker.sock" >> /etc/profile
